package mpi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mpi.actualizer.FileLine;
import mpi.actualizer.Story;
import mpi.map.MapperSource;
import mpi.g1.Algorithm;
import mpi.db.Database;
import mpi.repos.AttachmentRepo;
import mpi.repos.PatientRepo;
import mpi.statistics.Statistic;
import mpi.util.Excel;

import java.io.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Main {

    private final Database database = new Database();
    private AttachmentRepo ar;
    private PatientRepo pr;

    public static void main(String[] args) throws Exception {

        if(args != null && args.length != 0)
            if(args[0].equals("actualize"))
                new Main().start();
            else if(args[0].equals("statistic"))
                new Main().getStatistics();

    }

    public Main() {
        pr = new PatientRepo(database);
        ar = new AttachmentRepo(database);
    }

    public void start() throws Exception {

        Algorithm algorithm = new Algorithm();

        algorithm.setRepo(ar);
        algorithm.setQuarantine();


        final List<FileLine> patients = Excel.Parse("/tmp/patients.csv");
        final Set<String> enpList = getProcessed("/tmp/actions.log");

        final List<FileLine> onlyNotProcessedPatients = patients.stream().filter(e -> !enpList.contains(e.getPatient().getEnp())).collect(Collectors.toList());


        onlyNotProcessedPatients.parallelStream().forEach(this::proc);

        algorithm.undoQuarantine();
        algorithm.removeQuarantined();

        database.terminate();
    }

    private void proc(FileLine fl) throws RuntimeException {
        try {
            Story handler = null;
            handler = new Story();
            handler.setInput(fl);
            handler.setRepo(pr);
            handler.process();

        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void getStatistics() throws Exception {

        Statistic statistic;

        statistic = new Statistic();
        statistic.setRepo(ar);

        Gson gson = new GsonBuilder()
                .serializeNulls()
                .setPrettyPrinting()
                .create();

        gson.toJson(statistic.getSlice(), new FileWriter("/tmp/slice.json"));

        database.terminate();
    }

    private static Set<String> getProcessed(String uri) throws IOException {
        Set<String> processed =  new HashSet<>();

            File inputF = new File(uri);

            if(!inputF.exists()) {
                System.out.println("processed not exists");
                return processed;
            }

            InputStream inputFS = new FileInputStream(inputF);

            BufferedReader br = new BufferedReader(new InputStreamReader(inputFS));

            processed = br.lines().map(MapperSource::mapProcessed).collect(Collectors.toSet());

            br.close();

        return processed;
    }

}
