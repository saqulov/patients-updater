package mpi.repos;

import mpi.actualizer.FileLine;
import mpi.actualizer.Patient;
import mpi.g1.Algorithm;
import mpi.map.MapperSource;
import mpi.db.Database;
import mpi.statistics.Attachment;
import mpi.util.Loggers;

import java.io.IOException;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.*;

public class PatientRepo {
    private Database database;

    public PatientRepo(Database database) {
        this.database = database;
    }

    private static Long mapId(ResultSet resultSet) {

        Long id = (long) -1;

        try {
            if(resultSet.next())
                id = resultSet.getLong("id");
            else System.out.println("empty RS");

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        finally {
            return id;
        }
    }

    public List<Long> createPatient(FileLine fl) throws Exception {
        List<Long> ids = new ArrayList<>();

        //#region insert person
        String insertPerson = "insert into core.person " +
                "(create_date, event_id, deleted, last_name, first_name, middle_name, gender, birth_date, snils, enp) " +
                "values (current_timestamp, 1, FALSE, ?, ?, ?,?, ?, ?, ?)";

        List<Object> params = new ArrayList<Object>();

        params.add(fl.getPatient().getLastName());
        params.add(fl.getPatient().getFirstName());
        params.add(fl.getPatient().getMiddleName());
        params.add(fl.getPatient().getGender());
        params.add(fl.getPatient().getBirthDate());
        params.add(fl.getPatient().getSnils());
        params.add(fl.getPatient().getEnp());

        //long person = database.executeInsert(insertPerson, params);
        long person = database.executeInsert(insertPerson, params);
        ids.add(person);
        //#endregion

        //#region update hash
        params.clear();

        params.add(person);

        database.executeUpdate("update core.person " +
                "set hash = md5(upper(first_name || COALESCE(middle_name, '') || birth_date::text)) " +
                "where id = ?", params);
        //#endregion

        //#region get MO
//            params.clear();

//            params.add(fl.getSmo());
//
//            long mo = database.execute("select id from core.organization where code_foms = ? and deleted = FALSE", params, PatientRepo::mapId);
        //#endregion

        //#region insert attachment v1.0
//            if(fl.getAttach() != null && fl.getTypeAttach() != null) {
//                params.clear();
//
//                String insertAttachment = "insert into core.attachment " +
//                        "(create_date, event_id, deleted, date_in, date_out, type, mo, person, district, owner_snils) " +
//                        "values (current_timestamp, 1, FALSE, ?, ?, ?, ?, ?, ?, ?)";
//
//                params.add(fl.getAttach());
//                params.add(fl.getUnAttach());
//                params.add(fl.getTypeAttach());
//                params.add(mo);
//                params.add(person);
//                params.add(fl.getPlaceCode());
//                params.add(fl.getDocSNILS());
//
//                Long attachment = database.executeInsert(insertAttachment, params);
//                ids.add(attachment);
//            }
// #endregion

        //#region insert attachment v1.1
        if(fl.getAttach() != null && fl.getTypeAttach() != null) {
            createAttachment(fl, person);
        }
        //#endregion

        //#region insert insurance
        params.clear();

        String type = validatePolicyType(fl.getDpfsType());

        String insertInsurance = "insert into core.insurance " +
                "(create_date, event_id, deleted, person, type, begin_date, category, series, number) " +
                "values (current_timestamp, 1, FALSE, ?, ?, ?, ?, ?, ?)";

        params.add(person);
        params.add(type);
        params.add(fl.getStart());
        params.add("ОМС");
        params.add(type.equals("3") ? null : fl.getPolicySeries());
        params.add(type.equals("3") ? fl.getPatient().getEnp() : fl.getPolicyNumber());

        Long insurance = database.executeInsert(insertInsurance, params);
        ids.add(insurance);
        //#endregion

        return ids;
    }

    public Long getAttachment(FileLine line, Long person) throws Exception {
        List<Object> params = new ArrayList<>();
        params.add(person);

        Attachment e = database.executeSelect("select *\n" +
                "from core.attachment\n" +
                "inner join core.person_characteristics on person_characteristics.attachment = core.attachment.id\n" +
                "where core.attachment.person = ?\n", params, MapperSource::mapAttachment);

        params.clear();

        //search attachment by all
        if(Objects.equals(line.getPlaceCode(), e.getDistrict()) &&
                Objects.equals(e.getType(), line.getTypeAttach()) &&
                Objects.equals(e.getDateIn(), line.getAttach())) {
            if(!Objects.equals(e.getDoctor(), line.getDoctor()))
                updateOwnerSNILS(e.getId(), line.getDoctor());
            return 0L;
        } else if(person.equals(e.getPerson())) {
            if(line.getAttach() != null && line.getTypeAttach() != null)
                updateAttachment(line, person);
            return 0L;
        }

        return -1L;
    }

    private void updateOwnerSNILS(Long id, String doctor) throws
            IOException {
        List<Object> params = new ArrayList<>();

        params.add(doctor);
        params.add(id);

        database.executeUpdate("update core.attachment set owner_snils = ? where id = ?", params);

        Loggers.logQuarantined(String.format("op:ga;attachment:%s", id));
    }

    public Long updateAttachment(FileLine fl, Long id) throws Exception {
        List<Object> params = new ArrayList<>();
        params.add(id);

        database.executeUpdate("delete from core.person_characteristics where person = ?", params);

        Loggers.logQuarantined(String.format("op:ua; pid:%s", id));

        return createAttachment(fl, id);
    }

//    public Long getAttachment(Long id) {
//
//        List<Object> params = new ArrayList<>();
//        params.add(id);
//
//        String sql = "select attachment id\n" +
//                "from core.person_characteristics\n" +
//                "inner join core.attachment on person_characteristics.attachment = attachment.id\n" +
//                "where person_characteristics.person = ?";
//
//        return database.executeSelect(sql, params, PatientRepo::mapId);
//    }

    public Long createAttachment(FileLine fl, Long id) throws Exception {

        List<Object> params = new ArrayList<>();

        params.add(fl.getSmo());

        long mo = database.executeSelect("select id from core.organization where code_foms = ? and deleted = FALSE", params, PatientRepo::mapId);

        params.clear();

        params.add(fl.getAttach());
        params.add(fl.getUnAttach());
        params.add(fl.getTypeAttach());
        params.add(mo);
        params.add(id);
        params.add(fl.getPlaceCode());
        params.add(fl.getDoctor());

        Long attachment =  database.executeInsert("" +
                "insert into core.attachment " +
                "(create_date, event_id, date_in, date_out, type, mo, person, district, owner_snils)" +
                " values" +
                " (current_timestamp, 1, ?, ?, ?, ?, ?, ?, ?)", params);
        params.clear();

        params.add(id);
        params.add(attachment);

        Long characteristic = database.executeInsert("" +
                "insert into core.person_characteristics (person, attachment) values (?, ?)", params);

        Loggers.logQuarantined(String.format("op:ipc; person:%s attachment:%s characteristic:%s",
                id, attachment, characteristic));

        return attachment;
    }

    public void undoQuarantine(Long id) throws Exception {

        List<Object> params = new ArrayList<>();
        params.add(id);
        String sql = "update core.person_characteristics set quarantine = false where person = ?";

        Loggers.logQuarantined(String.format("op:uq;person:%s", id));

        database.executeUpdate(sql, params);
    }

    public void undoQuarantineV2(Long id) throws Exception {

        Loggers.logQuarantined(String.format("op:uq;person:%s", id));
        Algorithm.toQuarantined.add(id);
    }

    private String validatePolicyType(String type) {
        if(type == null)
            return "3";

        return type.equals("С") ? "1" :
                type.equals("В") ? "2" : "3";
    }
    public List<Patient> getPatientByENP(String enp) {
        String query = "SELECT id," +
                "              last_name," +
                "              first_name," +
                "              middle_name," +
                "              gender," +
                "              birth_date," +
                "              snils," +
                "              enp" +
                "       FROM core.person" +
                "       WHERE enp = ?";

        List<Object> params = new ArrayList<Object>();

        params.add(enp);

        return database.executeSelect(query, params, MapperSource::MapPatients);
    }
    public List<Patient> getPatientBySNILS(String snils) {
        String query = "SELECT id," +
                "              last_name," +
                "              first_name," +
                "              middle_name," +
                "              gender," +
                "              birth_date," +
                "              snils," +
                "              enp" +
                "       FROM core.person" +
                "       WHERE snils = ?";

        List<Object> params = new ArrayList<Object>();

        params.add(snils);

        return database.executeSelect(query, params, MapperSource::MapPatients);
    }
    public List<Patient> getPatientByBase(LocalDate birthDate, String firstName,
                                          String middleName) {

        String query = "SELECT id, " +
                "last_name, " +
                "first_name, " +
                "middle_name, " +
                "gender, " +
                "birth_date, " +
                "snils, " +
                "enp " +
                "FROM core.person " +
                "WHERE hash = md5(upper(? || COALESCE(?, '') || ?::text))";

        List<Object> params = new ArrayList<Object>();

        params.add(firstName);
        params.add(middleName);
        params.add(birthDate);

        return database.executeSelect(query, params, MapperSource::MapPatients);
    }

//    public void commitStoryQueries() throws SQLException {
//        database.executeTransaction();
//    }
}
