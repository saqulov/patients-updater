package mpi.repos;

import mpi.g1.PersonCharacteristics;
import mpi.db.Database;
import mpi.map.MapperSource;
import mpi.statistics.AttachmentSlice;
import mpi.statistics.District;
import mpi.statistics.MO;
import mpi.util.Loggers;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Optional.ofNullable;

public class AttachmentRepo {
    private Database database;

    public AttachmentRepo(Database database) {
        this.database = database;
    }

    public AttachmentSlice createSlice() throws RuntimeException {

        AttachmentSlice aSlice = new AttachmentSlice();
        List<Object> params = new ArrayList<>();

        //#region get MOs
        String org = "" +
                "select id," +
                " name," +
                " short_name," +
                " oid," +
                " code_foms" +
                " from core.organization" +
                " where deleted = false";

        aSlice.setMos(database.executeSelect(org, params, MapperSource::mapMOs));
        //#endregion

        //#region get districts by MO v1.2
        aSlice.getMos().parallelStream().forEach(this::processMO);
        //endregion

        //#region get districts by MO 1.0
//        for (MO mo : aSlice.getMos()) {
//            String district = "" +
//                    "select distinct district " +
//                    "from core.attachment a " +
//                    "inner join core.person_characteristics pc on pc.attachment = a.id " +
//                    "where district like '%_' || ? " +
//                    "and a.deleted = false";
//
//            params.add(mo.getCodeFOMS());
//
//            mo.setDistricts(database.execute(district, params, MapperSource::mapDistricts));
//
//            params.clear();
//
//            //#region get mpi.attachments by district
//
////            mo.getDistricts().parallelStream().forEach(this::h1);
//
//            for (District d : mo.getDistricts()) {
//                String attachment = "" +
//                        "select count(*) size" +
//                        " from core.attachment a " +
//                        " inner join core.person_characteristics pc on pc.attachment = a.id" +
//                        " where district = ?" +
//                        " and deleted = false";
//
//                params.add(d.getName());
//
//                d.setSize(database.execute(attachment, params, MapperSource::mapAttachmentsSize));
//
//                params.clear();
//            }
//
//            params.clear();
//        }
//
//        //#endregion

        return aSlice;
    }

    private void processDistrict(District d) {
        List<Object> params = new ArrayList<>();

        String attachment = "" +
                "select count(*) size" +
                " from core.attachment a " +
                " inner join core.person_characteristics pc on pc.attachment = a.id" +
                " where district = ?" +
                " and deleted = false";

        params.add(d.getName());

        d.setSize(database.executeSelect(attachment, params, MapperSource::mapAttachmentsSize));

        params = null;
    }

    private void processMO(MO mo) {
        List<Object> params = new ArrayList<>();

        String district = "" +
                "select distinct district " +
                "from core.attachment a " +
                "inner join core.person_characteristics pc on pc.attachment = a.id " +
                "where district like '%_' || ? " +
                "and a.deleted = false";

        params.add(mo.getCodeFOMS());

        mo.setDistricts(database.executeSelect(district, params, MapperSource::mapDistricts));

        params = null;

        mo.getDistricts().parallelStream().forEach(this::processDistrict);
    }

    public Long setQuarantine() throws IOException {

        Long affected = database.executeUpdate("" +
                "update core.person_characteristics set quarantine = true", Collections.emptyList());
        String log = "op:sq;affected:" + affected;
        Loggers.logQuarantined(log);

        return affected;
    }

    public Long removeQuarantined() throws IOException {

        String sql = "" +
                "select * from core.person_characteristics where quarantine = true";

        List <PersonCharacteristics> pcList = database.executeSelect(sql, Collections.emptyList(), MapperSource::mapPersonCharacteristics);

        String log = "op:rq;" + ofNullable(pcList)
                .map(Collection::stream)
                .orElseGet(Stream::empty).map(pc -> String.format("id:%d pid:%d aid:%d",
                        pc.getId(),
                        pc.getPerson(),
                        pc.getAttachment())).collect(Collectors.joining(";"));

        Loggers.logQuarantined(log);

        return database.executeUpdate("" +
                "delete from core.person_characteristics where quarantine = true" , Collections.emptyList());
    }

    public void undoQuarantineBatch(Set<Long> quarantined) throws RuntimeException {
     database.updateBatch("update core.person_characteristics set quarantine = false where person = ?", quarantined);
    }

}
