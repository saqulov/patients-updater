package mpi.repos;

import mpi.db.Database;

public final class Repositories {

    private AttachmentRepo attachmentRepo;
    private PatientRepo patientRepo;
    private Database db;

    private static volatile Repositories instance;

    public static Repositories get(Database database) {

        Repositories result = instance;

        if (result != null) {
            return result;
        }
        synchronized(Repositories.class) {
            if (instance == null) {
                instance = new Repositories(database);
            }
            return instance;
        }
    }

    private Repositories(Database database) {
        db = database;
        attachmentRepo = new AttachmentRepo(db);
        patientRepo = new PatientRepo(db);
    }
    public AttachmentRepo getAttachmentRepo() {
        return attachmentRepo;
    }
    public PatientRepo getPatientRepo() {
        return patientRepo;
    }

}

