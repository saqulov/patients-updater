package mpi.map;

import mpi.actualizer.Patient;
import mpi.g1.PersonCharacteristics;
import mpi.statistics.Attachment;
import mpi.statistics.District;
import mpi.statistics.MO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MapperSource {
    private static final Logger LOG = LoggerFactory.getLogger(MapperSource.class);

    private static final String MAPPING_PATIENT_ERROR = "Ошибка при маппинге пациента";

    public static List<Patient> MapPatients(ResultSet rs) {
        try {
            List<Patient> patients = new ArrayList<>();

            while(rs.next()){
                Patient patient = new Patient();

                patient.setEnp(rs.getString("enp"));

                String lastName = rs.getString("last_name");
                if(lastName != null && !lastName.trim().isEmpty()) {
                    patient.setLastName(lastName.trim());
                }

                String firstName = rs.getString("first_name");
                if(firstName != null && !firstName.trim().isEmpty()) {
                    patient.setFirstName(firstName.trim());
                }

                String middleName = rs.getString("middle_name");
                if(middleName != null && !middleName.trim().isEmpty()) {
                    patient.setMiddleName(middleName.trim());
                }

                patient.setBirthDate(rs.getDate("birth_date").toLocalDate());
                patient.setGender(rs.getString("gender"));

                String snils = rs.getString("snils");
                if(snils != null && !snils.trim().isEmpty()) {
                    patient.setSnils(snils.trim());
                }

                Long id = rs.getLong("id");
                patient.setId(id);

                patients.add(patient);
            }

            return patients;
        } catch (Exception e) {
            LOG.error(MAPPING_PATIENT_ERROR, e);
            return null;
        }
    }

    public static List<MO> mapMOs(ResultSet rs) throws RuntimeException {
        List<MO> mos = new ArrayList<>();
        try {
            while(rs.next()) {
                MO mo =  new MO();
                mo.setId(rs.getLong("id"));
                mo.setName(rs.getString("name"));
                mo.setShortName(rs.getString("short_name"));
                mo.setOid(rs.getString("oid"));
                mo.setCodeFOMS(rs.getString("code_foms"));

                mos.add(mo);
            }
        } catch (SQLException exception) {
            throw new RuntimeException(exception.getMessage());
        }

        return mos;
    }

    public static List<District> mapDistricts(ResultSet rs) throws RuntimeException {

        List<District> districts = new ArrayList<>();
        try {
            while(rs.next()) {
                District district =  new District();
                district.setName(rs.getString("district"));
                districts.add(district);
            }
        } catch (SQLException exception) {
            throw new RuntimeException(exception.getMessage());
        }

        return districts;
    }

    public static Attachment mapAttachment(ResultSet rs) throws RuntimeException {
        Attachment attachment =  new Attachment();
        try {
            while(rs.next()) {

                attachment.setId(rs.getLong("id"));
                attachment.setCreateDate(rs.getTimestamp("create_date").toLocalDateTime());

                if (rs.getObject("update_date") != null)
                    attachment.setUpdateDate(rs.getTimestamp("update_date").toLocalDateTime());

                attachment.setEventId(rs.getLong("event_id"));
                attachment.setDeleted(rs.getBoolean("deleted"));
                attachment.setDateIn(rs.getTimestamp("date_in").toLocalDateTime());

                if (rs.getObject("date_out") != null)
                    attachment.setDateOut(rs.getTimestamp("update_date").toLocalDateTime());

                if (rs.getObject("owner_snils") != null)
                    attachment.setDoctor(rs.getString("owner_snils"));

                if(rs.getObject("district") != null)
                    attachment.setDistrict(rs.getString("district"));

                attachment.setPerson(rs.getLong("person"));
                attachment.setType(rs.getInt("type"));
            }
        } catch (SQLException exception) {
            throw new RuntimeException(exception.getMessage());
        }

        return attachment;
    }

    public static long mapAttachmentsSize(ResultSet rs) throws RuntimeException {

        long size = 0;
        try {
            while(rs.next()) {
                size = rs.getLong("size");
            }
        } catch (SQLException exception) {
            throw new RuntimeException(exception.getMessage());
        }

        return size;
    }

    public static String mapProcessed(String line) {
        try {
            String data = line.split(";")[1];

            String patient = data.replaceFirst("data:", "");

            String enp = patient.split(" ")[7].replaceAll("enp:", "");

            return enp;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return "";
        }
    }

    public static List<PersonCharacteristics> mapPersonCharacteristics(ResultSet rs) {
        List<PersonCharacteristics> pcList = new ArrayList<>();

        try {
            while(rs.next()) {
                PersonCharacteristics pc = new PersonCharacteristics();
                pc.setId(rs.getLong("id"));
                pc.setAttachment(rs.getLong("attachment"));
                pc.setPerson(rs.getLong("person"));
                pcList.add(pc);
            }
        } catch (SQLException exception) {
            throw new RuntimeException(exception.getMessage());
        }

        return pcList;
    }

}
