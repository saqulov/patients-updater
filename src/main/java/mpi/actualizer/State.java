package mpi.actualizer;

public abstract class State {

    //#region data states
    public final static String EMPTY_DATA = "Поиск невозможен - не предоставлено ни одно из ключевых полей.";
    public final static String DAMAGED_DATA = "";
    public final static String COMPLETE_DATA = "Поиск успешно инициирован - все ключевые поля предоставлены.";
    public final static String DAMAGED_DATA_ENP = "Поиск невозможен - поле 'ЕНП' обязательно к заполнению.";
    public final static String DAMAGED_DATA_FIRST = "Поиск невозможен - поле 'Имя' обязательно к заполнению.";
    public final static String DAMAGED_DATA_DATE = "Поиск невозможен - поле 'Дата рождения' обязательно к заполнению.";
    public final static String DAMAGED_DATA_GENDER = "Поиск невозможен - поле 'Пол' обязательно к заполнению.";
    public final static String DATA_SNILS_NOT_REPRESENTED = "СНИЛС отсутствует - поиск по данному критерию невозможен.";
    //#endregion

    //#region search states
    public final static String NOT_FOUND_BY_ENP = "Не удалось обнаружить записи по критерию 'ЕНП'";
    public final static String DUPLICATE_BY_ENP = "Обнаружено >1 записей по критерию 'ЕНП'";
    public final static String FOUND_BY_ENP = "Обнаружена одна запись по критерию 'ЕНП'";
    public final static String NOT_FOUND_BY_SNILS = "Не удалось обнаружить записи по критерию 'СНИЛС'";
    public final static String DUPLICATE_BY_SNILS = "Обнаружено >1 записей по критерию 'СНИЛС'";
    public final static String  FOUND_BY_SNILS = "Обнаружена одна запись по критерию 'СНИЛС'";
    public final static String FOUND_BY_BASE = "Обнаружена одна запись по критерию 'Имя + Отчество + ДР + Пол'";
    public final static String NOT_FOUND_BY_BASE = "Не удалось обнаружить записи по критерию 'Имя + Отчество + ДР + Пол'";
    public final static String DUPLICATE_BY_BASE = "Обнаружено >1 записей по критерию 'Имя + Отчество + ДР + Пол'";
    //#endregion

    //#region merge state
    public final static String FOUND_BY_ALL = "Обнаружена одна запись, совпадающая по всем критериям.";
    public final static String NOT_FOUND_BY_ALL = "Не обнаружены записи, совпадающие по всем критериям.";
    public final static String DUPLICATE_BY__ALL = "Обнаружено слишком много записей, совпадающих по всем критериям.";
    public final static String FOUND_BY_ENP_BASE = "Обнаружена одна запись, совпадающая по критериям: 'ЕНП, Имя, Отчество, ДР, Пол'.";
    public final static String NOT_FOUND_BY_ENP_BASE = "Не обнаружены записи, совпадающие по критериям: 'ЕНП, Имя, Отчество, ДР, Пол'.";
    public final static String DUPLICATE_BY_ENP_BASE = "Обнаружено слишком много записей, совпадающих по критериям: 'ЕНП, Имя, Отчество, ДР, Пол'.";
    public final static String FOUND_BY_SNILS_BASE = "Обнаружена одна запись, совпадающая по критериям: 'СНИЛС, Имя, Отчество, ДР, Пол'.";
    public final static String NOT_FOUND_BY_SNILS_BASE = "Не обнаружены записи, совпадающие по критериям: 'СНИЛС, Имя, Отчество, ДР, Пол'";
    public final static String DUPLICATE_BY_SNILS_BASE = "Обнаружено слишком много записей, совпадающих по критериям: 'СНИЛС, Имя, Отчество, ДР, Пол'.";
    //#endregion

    //#region solution state
    public final static String ERROR = "Ошибка";
    public final static String UPDATE = "Обновление";
    public final static String UPDATE_ENP = "Обновить ЕНП и необязательные поля";
    public final static String UPDATE_SNILS = "Обновить СНИЛС и необязательные поля";
    public final static String ADD = "Добавить";
    public final static String PULL_UP = "Выставить контрольную запись в самую свежую";
    //#endregion
}
