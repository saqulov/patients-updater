package mpi.actualizer;

public enum Stage {
    VALIDATION,
    LOADING,
    SEARCHING,
    INTERSECT,
    DECIDING,
    MAKING
}
