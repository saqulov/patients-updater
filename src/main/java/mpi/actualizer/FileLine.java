package mpi.actualizer;

import java.time.LocalDateTime;

public class FileLine {
    private Patient patient;
    private String action;
    private String birthPlace;
    private String registryAddress;
    private String address;
    private String registryFias;
    private String fias;
    private String dpfsType;
    private String policySeries;
    private String policyNumber;
    private LocalDateTime start;
    private String smo;
    private Integer typeAttach;
    private LocalDateTime attach;
    private LocalDateTime unAttach;
    private String placeCode;
    private String doctor;

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getRegistryAddress() {
        return registryAddress;
    }

    public void setRegistryAddress(String registryAddress) {
        this.registryAddress = registryAddress;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {

        this.address = address;
    }

    public String getFias() {
        return fias;
    }

    public void setFias(String fias) {
        this.fias = fias;
    }

    public String getRegistryFias() {
        return registryFias;
    }

    public void setRegistryFias(String fias) {
        this.registryFias = fias;
    }

    public String getDpfsType() {
        return dpfsType;
    }

    public void setDpfsType(String dpfsType) {
        this.dpfsType = dpfsType;
    }

    public String getPolicySeries() {
        return policySeries;
    }

    public void setPolicySeries(String policySeries) {
        this.policySeries = policySeries;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public String getSmo() {
        return smo;
    }

    public void setSmo(String smo) {
        this.smo = smo;
    }

    public Integer getTypeAttach() {
        return typeAttach;
    }

    public void setTypeAttach(Integer typeAttach) {
        this.typeAttach = typeAttach;
    }

    public LocalDateTime getAttach() {
        return attach;
    }

    public void setAttach(LocalDateTime attach) {
        this.attach = attach;
    }

    public LocalDateTime getUnAttach() {
        return unAttach;
    }

    public void setUnAttach(LocalDateTime unAttach) {
        this.unAttach = unAttach;
    }

    public String getPlaceCode() {
        return placeCode;
    }

    public void setPlaceCode(String placeCode) {
        this.placeCode = placeCode;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDocSNILS(String doctor) {
        this.doctor = doctor;
    }
}
