package mpi.actualizer;

public class Mutation {
    private String state;
    private Stage stage;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public String toString() {
        return String.valueOf(stage) + " " + state;
    }
}