package mpi.actualizer;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

public class Patient {
    private Long id;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
    private boolean deleted;
    private String lastName;
    private String firstName;
    private String middleName;
    private String gender;
    private LocalDate birthDate;
    private String snils;
    private String enp;
    private boolean dead;
    private LocalDate deathDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getSnils() {
        return snils;
    }

    public void setSnils(String snils) {
        this.snils = snils;
    }

    public String getEnp() {
        return enp;
    }

    public void setEnp(String enp) {
        this.enp = enp;
    }

    public boolean isDead() {
        return dead;
    }

    public void setDead(boolean dead) {
        this.dead = dead;
    }

    public LocalDate getDeathDate() {
        return deathDate;
    }

    public void setDeathDate(LocalDate deathDate) {
        this.deathDate = deathDate;
    }

    @Override
    public String toString() {
        return String.format("id:%s last:%s first:%s middle:%s gender:%s birth:%s snils:%s enp:%s",
                id,
                lastName,
                firstName,
                middleName,
                gender,
                birthDate,
                snils,
                enp);
    }
    public static boolean isNullOrEmpty(String str){
        return str == null || str.trim().length() == 0;
    }

    public static boolean isCompleteData(Patient dto){
        return ( (dto.birthDate != null) &&
                (dto.firstName != null && dto.firstName.trim().length() != 0) &&
                (dto.gender != null && dto.gender.trim().length() != 0) &&
                (dto.middleName != null && dto.middleName.trim().length() != 0) &&
                (dto.lastName != null && dto.lastName.trim().length() != 0) &&
                (dto.enp != null && dto.enp.trim().length() != 0) &&
                (dto.snils != null && dto.snils.trim().length() != 0));
    }
    public static boolean isPartialData(Patient dto){
        return ( dto.birthDate == null ||
                isNullOrEmpty(dto.firstName) ||
                isNullOrEmpty(dto.gender) ||
                isNullOrEmpty(dto.middleName) ||
                isNullOrEmpty(dto.lastName) ||
                isNullOrEmpty(dto.enp) ||
                isNullOrEmpty(dto.snils)) && !isNullEmptyData(dto);
    }
    public static boolean isNullEmptyData(Patient dto){
        if(dto == null)
            return true;

        return ( (dto.birthDate == null) &&
                (isNullOrEmpty(dto.firstName)) &&
                (isNullOrEmpty(dto.gender)) &&
                (isNullOrEmpty(dto.middleName)) &&
                (isNullOrEmpty(dto.lastName)) &&
                (isNullOrEmpty(dto.enp)) &&
                (isNullOrEmpty(dto.snils)));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Patient)) return false;
        Patient patient = (Patient) o;
        return firstName.toUpperCase().equals(patient.firstName.toUpperCase()) &&
                ( (middleName == null && patient.middleName == null) ||
                        ( (middleName != null && patient.middleName != null)
                                && Objects.equals(middleName.toUpperCase(), patient.middleName.toUpperCase())) ) &&
                birthDate.equals(patient.birthDate) &&
                Objects.equals(snils, patient.snils) &&
                Objects.equals(enp, patient.enp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName.toUpperCase(), middleName.toUpperCase(), birthDate, snils, enp);
    }
}

