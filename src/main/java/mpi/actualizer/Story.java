package mpi.actualizer;

import mpi.repos.PatientRepo;
import mpi.util.LogBuilder;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Optional.ofNullable;

public class Story {

    protected  boolean stopStoryWritingWithError;



    private LogBuilder logBuilder = new LogBuilder();
    private List<Mutation> story =  new ArrayList<>();
    private FileLine input;
    private PatientRepo repo;

    private List<Patient> cachedByENP;
    private List<Patient> cachedBySNILS;
    private List<Patient> cachedByBase;

    private List<Patient> filteredByAll;
    private List<Patient> filteredByBaseAndSNILS;
    private List<Patient> filteredByBaseAndENP;

    //public static Set<Long> notCheckedAttachmentOf = new HashSet<>();

    private boolean isSNILSNotProvided;


    public Story () {
    }

    public void setInput(FileLine input) {
        this.input = input;
        logBuilder.append(input.getPatient());
    }

    public void setRepo(PatientRepo repo) {
        this.repo = repo;
    }
    public void tell() {
        if(stopStoryWritingWithError)
            logBuilder.append("Обратите внимание! Обновление записей закончилось ошибкой.");

        logBuilder.storyToFile();
        logBuilder.operationsToFile();
    }

    public void update(String state, Stage stage) {
        Mutation tmp = new Mutation();
        tmp.setStage(stage);
        tmp.setState(state);

        story.add(tmp);

        logBuilder.append(tmp.toString());
    }

    public void update(String state, Stage stage, List<Patient> patients) {
        Mutation tmp = new Mutation();

        tmp.setStage(stage);
        tmp.setState(state);

        story.add(tmp);

        String dstState = tmp + " id: [" + ofNullable(patients)
                .map(Collection::stream)
                .orElseGet(Stream::empty).map(e -> "" + e.getId()).collect(Collectors.joining(",")) + "]";

        logBuilder.append(dstState);

        if(patients != null) patients.forEach(logBuilder::append);
    }


    public void process() throws Exception {
        validate();
        load();
        simpleFilter();
        intersectFilter();
        decide();
        make();
        tell();
    }

    private void validate() {

        if(Patient.isNullEmptyData(input.getPatient())) {
            update(State.EMPTY_DATA, Stage.VALIDATION);
            stopStoryWritingWithError = true;
            return;
        }
        else if(Patient.isCompleteData(input.getPatient())) {
            update(State.COMPLETE_DATA, Stage.VALIDATION);
        }
        else {
            if(input.getPatient().getEnp() == null || input.getPatient().getEnp().trim().length() == 0) {
                update(State.DAMAGED_DATA_ENP, Stage.VALIDATION);
                stopStoryWritingWithError = true;
                return;
            }
            if(input.getPatient().getFirstName() == null || input.getPatient().getFirstName().trim().length() == 0) {
                update(State.DAMAGED_DATA_FIRST, Stage.VALIDATION);
                stopStoryWritingWithError = true;
                return;
            }
            if(input.getPatient().getBirthDate() == null) {
                update(State.DAMAGED_DATA_DATE, Stage.VALIDATION);
                stopStoryWritingWithError = true;
                return;
            }
            if(input.getPatient().getGender() == null) {
                update(State.DAMAGED_DATA_GENDER, Stage.VALIDATION);
                stopStoryWritingWithError = true;
                return;
            }
            isSNILSNotProvided = input.getPatient().getSnils() == null || input.getPatient().getSnils().trim().length() == 0;
        }
    }

    private void loadByENP() {
        cachedByENP = repo.getPatientByENP(input.getPatient().getEnp());

        if(cachedByENP == null) cachedByENP = Collections.emptyList();
    }

    private void loadBySNILS() {
        if (!isSNILSNotProvided) {
            cachedBySNILS = repo.getPatientBySNILS(input.getPatient().getSnils());
        } else update(State.DATA_SNILS_NOT_REPRESENTED, Stage.VALIDATION);

        if(cachedBySNILS == null) cachedBySNILS = Collections.emptyList();
    }

    private void loadByName() {

        cachedByBase = repo.getPatientByBase(input.getPatient().getBirthDate(),
                input.getPatient().getFirstName(), input.getPatient().getMiddleName());

        if(cachedByBase == null) cachedByBase = Collections.emptyList();
    }

    private void simpleFilter() {
        if(stopStoryWritingWithError)
            return;

        if(cachedByENP.isEmpty()) update(State.NOT_FOUND_BY_ENP, Stage.SEARCHING);
        else {
            if (cachedByENP.size() == 1) update(State.FOUND_BY_ENP, Stage.SEARCHING, cachedByENP);
            else update(State.DUPLICATE_BY_ENP, Stage.SEARCHING, cachedByENP);
        }

        if(cachedBySNILS.isEmpty())
            update(State.NOT_FOUND_BY_SNILS, Stage.SEARCHING, cachedBySNILS);
        else {
            if (cachedBySNILS.size() == 1) update(State.FOUND_BY_SNILS, Stage.SEARCHING, cachedBySNILS);
            else update(State.DUPLICATE_BY_SNILS, Stage.SEARCHING, cachedByENP);
        }

        if(cachedByBase.isEmpty()) update(State.NOT_FOUND_BY_BASE, Stage.SEARCHING, cachedByBase);
        else {
            if (cachedByBase.size() == 1) update(State.FOUND_BY_BASE, Stage.SEARCHING, cachedByBase);
            else update(State.DUPLICATE_BY_BASE, Stage.SEARCHING, cachedByBase);
        }
    }

    private void load() {
        if(stopStoryWritingWithError)
            return;

        loadByENP();
        loadByName();
        loadBySNILS();
    }

    private void intersectFilter() {
        if(stopStoryWritingWithError)
            return;

        filteredByAll = cachedByBase.stream()
                .filter(cachedBySNILS::contains)
                .collect(Collectors.toList())
                .stream().filter(cachedByENP::contains)
                .collect(Collectors.toList());

        if(filteredByAll.isEmpty()) {
            update(State.NOT_FOUND_BY_ALL, Stage.INTERSECT);

        } else if (filteredByAll.size() > 1) {
            update(State.DUPLICATE_BY__ALL, Stage.INTERSECT, filteredByAll);
        } else {
            update(State.FOUND_BY_ALL, Stage.INTERSECT, filteredByAll);
        }

        filteredByBaseAndSNILS = cachedByBase.stream()
                .filter(cachedBySNILS::contains)
                .collect(Collectors.toList());

        if(filteredByBaseAndSNILS.isEmpty()) update(State.NOT_FOUND_BY_SNILS_BASE, Stage.INTERSECT);
        else {
            if (filteredByBaseAndSNILS.size() > 1) update(State.DUPLICATE_BY_SNILS_BASE, Stage.INTERSECT, filteredByBaseAndSNILS);
            else update(State.FOUND_BY_SNILS_BASE, Stage.INTERSECT, filteredByBaseAndSNILS);
        }

        filteredByBaseAndENP = cachedByENP.stream()
                .filter(cachedByBase::contains)
                .collect(Collectors.toList());

        if(filteredByBaseAndENP.isEmpty()) update(State.NOT_FOUND_BY_ENP_BASE, Stage.INTERSECT);
        else {
            if (filteredByBaseAndENP.size() > 1) update(State.DUPLICATE_BY_ENP_BASE, Stage.INTERSECT, filteredByBaseAndENP);
            else update(State.FOUND_BY_ENP_BASE, Stage.INTERSECT, filteredByBaseAndENP);
        }
    }

    private void decide() {
        if(stopStoryWritingWithError)
            return;

        if(story.stream().anyMatch(e -> e.getState().equals(State.FOUND_BY_ALL))) {
            update(State.UPDATE, Stage.DECIDING);
        } else if (story.stream().anyMatch(e -> e.getState().equals(State.NOT_FOUND_BY_ALL)) &&
                story.stream().anyMatch(e -> e.getState().equals(State.NOT_FOUND_BY_ENP_BASE)) &&
                story.stream().anyMatch(e -> e.getState().equals(State.DATA_SNILS_NOT_REPRESENTED)) &&
                story.stream().anyMatch(e -> e.getState().equals(State.FOUND_BY_ENP))) {
            update(State.ERROR, Stage.DECIDING);
        }else if(story.stream().anyMatch(e -> e.getState().equals(State.NOT_FOUND_BY_ALL)) &&
                story.stream().anyMatch(e -> e.getState().equals(State.NOT_FOUND_BY_ENP_BASE)) &&
                story.stream().anyMatch(e -> e.getState().equals(State.NOT_FOUND_BY_SNILS_BASE)) &&
                story.stream().anyMatch(e -> e.getState().equals(State.FOUND_BY_SNILS)) &&
                story.stream().anyMatch(e -> e.getState().equals(State.FOUND_BY_ENP))) {
            update(State.ERROR, Stage.DECIDING);
        } else if(story.stream().anyMatch(e -> e.getState().equals(State.NOT_FOUND_BY_ALL)) &&
                story.stream().anyMatch(e -> e.getState().equals(State.NOT_FOUND_BY_ENP_BASE)) &&
                story.stream().anyMatch(e -> e.getState().equals(State.NOT_FOUND_BY_SNILS_BASE))) {
            update(State.ADD, Stage.DECIDING);
        } else if(story.stream().anyMatch(e -> e.getState().equals(State.NOT_FOUND_BY_ALL)) &&
                story.stream().anyMatch(e -> e.getState().equals(State.FOUND_BY_ENP_BASE)) &&
                story.stream().anyMatch(e -> e.getState().equals(State.NOT_FOUND_BY_SNILS_BASE))) {
            update(State.UPDATE_SNILS, Stage.DECIDING);
        } else if(story.stream().anyMatch(e -> e.getState().equals(State.NOT_FOUND_BY_ALL)) &&
                story.stream().anyMatch(e -> e.getState().equals(State.NOT_FOUND_BY_ENP_BASE)) &&
                story.stream().anyMatch(e -> e.getState().equals(State.FOUND_BY_SNILS_BASE))) {
            update(State.UPDATE_ENP, Stage.DECIDING);
        } else if(story.stream().anyMatch(e -> e.getState().equals(State.DUPLICATE_BY_SNILS_BASE)) &&
                !story.stream().anyMatch(e -> e.getState().equals(State.DUPLICATE_BY_ENP_BASE))) {
            update(State.PULL_UP, Stage.DECIDING);
        } else  {
            update(State.ERROR, Stage.DECIDING);
        }
    }

    private void make() throws Exception {
        if(stopStoryWritingWithError)
            return;

        if(story.stream().anyMatch(e -> e.getState().equals(State.UPDATE))) {
            logBuilder.onUpdate("op:update;data:" + filteredByAll.get(0) + ";to " + input.getPatient());
            checkupAttachment(input, filteredByAll.get(0));
        } else if(story.stream().anyMatch(e -> e.getState().equals(State.UPDATE_SNILS))) {
            logBuilder.onUpdate("op:update_snils;data:" + filteredByBaseAndENP.get(0) + ";to " + input.getPatient());
            checkupAttachment(input, filteredByBaseAndENP.get(0));
        } else if(story.stream().anyMatch(e -> e.getState().equals(State.UPDATE_ENP))) {
            logBuilder.onUpdate("op:update_enp;data:" + filteredByBaseAndSNILS.get(0) + ";to " + input.getPatient());
            checkupAttachment(input, filteredByBaseAndSNILS.get(0));
        } else if(story.stream().anyMatch(e -> e.getState().equals(State.ADD))) {
            List<Long> ids = repo.createPatient(input);
            logBuilder.onUpdate("op:add;data:" + input.getPatient() + ";ids:" + ids.stream().map(e -> "" + e)
                    .collect(Collectors.joining(",")));
        }  else if(story.stream().anyMatch(e -> e.getState().equals(State.PULL_UP))) {
            logBuilder.onUpdate("op:pull_up_snils;data:" + filteredByBaseAndSNILS.get(filteredByBaseAndSNILS.size() - 1) +";to" + input.getPatient());
            logBuilder.onUpdate("crash records;" + filteredByBaseAndSNILS.stream().map(Patient::toString).collect(Collectors.joining(";")));
        } else if(story.stream().anyMatch(e -> e.getState().equals(State.ERROR))) {
            logBuilder.onUpdate("op:error;" +
                    "data:" + input.getPatient() + ";" +
                    "suspectsByENPWithBase:[" + ofNullable(filteredByBaseAndENP)
                    .map(Collection::stream)
                    .orElseGet(Stream::empty).map(e -> "" + e.getId()).collect(Collectors.joining(",")) + "]" +
                    " suspectsBySNILSWithBase:[" + ofNullable(filteredByBaseAndSNILS)
                    .map(Collection::stream)
                    .orElseGet(Stream::empty).map(e -> "" + e.getId()).collect(Collectors.joining(",")) + "]" +
                    " suspectsBySNILS:[" + ofNullable(cachedBySNILS)
                    .map(Collection::stream)
                    .orElseGet(Stream::empty).map(e -> "" + e.getId()).collect(Collectors.joining(",")) + "]"  +
                    " suspectsByENP:[" + ofNullable(cachedByENP)
                    .map(Collection::stream)
                    .orElseGet(Stream::empty).map(e -> "" + e.getId()).collect(Collectors.joining(",")) + "]"   +
                    " suspectsByBase:[" + ofNullable(cachedByBase)
                    .map(Collection::stream)
                    .orElseGet(Stream::empty).map(e -> "" + e.getId()).collect(Collectors.joining(",")) + "]");

//            notCheckedAttachmentOf.addAll(ofNullable(filteredByBaseAndENP)
//                    .map(Collection::stream)
//                    .orElseGet(Stream::empty).map(Patient::getId).collect(Collectors.toSet()));
//
//            notCheckedAttachmentOf.addAll(ofNullable(filteredByBaseAndSNILS)
//                    .map(Collection::stream)
//                    .orElseGet(Stream::empty).map(Patient::getId).collect(Collectors.toSet()));
//
//            notCheckedAttachmentOf.addAll(ofNullable(cachedByENP)
//                    .map(Collection::stream)
//                    .orElseGet(Stream::empty).map(Patient::getId).collect(Collectors.toSet()));
//
//            notCheckedAttachmentOf.addAll(ofNullable(cachedBySNILS)
//                    .map(Collection::stream)
//                    .orElseGet(Stream::empty).map(Patient::getId).collect(Collectors.toSet()));
        }
    }

    private void checkupAttachment(FileLine line, Patient patient) throws RuntimeException {
        try {
            Long existedAttachment = repo.getAttachment(line, patient.getId());

            if(existedAttachment != -1L) {
                //repo.undoQuarantine(patient.getId());
                repo.undoQuarantineV2(patient.getId());
            } else {
                if(line.getAttach() != null && line.getTypeAttach() != null)
                    repo.createAttachment(line, patient.getId());
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }

    }
}

