package mpi.util;

import mpi.actualizer.Patient;

import java.io.IOException;

public class LogBuilder {
    private StringBuilder storyBuilder = new StringBuilder();
    private StringBuilder operationBuilder = new StringBuilder();

    public void append(String str) {
        storyBuilder.append(" {" + str + "} ");
    }

    public void append(Patient p) {
        append(" data:{" + p + "} ");
    }

    public void onUpdate(String data) {

        operationBuilder.append(data + "\n");
    }
    @Override
    public String toString() {
        return String.format("\n{ %s }", storyBuilder.toString());
    }

    public void storyToFile() {
        try {
            Loggers.logProcess(this.toString());

        } catch (IOException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException();
        }
    }

    public void operationsToFile() {
        try {
            Loggers.logActions(operationBuilder.toString());

        } catch (IOException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException();
        }
    }

}