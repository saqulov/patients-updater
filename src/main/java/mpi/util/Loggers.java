package mpi.util;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public abstract class Loggers {

    public static void logProcess(String data)
            throws IOException {
        File file = new File(File.separator + "tmp" + File.separator + "processes.log");
        try ( BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), StandardCharsets.UTF_8)) ) {
            bw.write(data);
        }
    }

    public static void logActions(String data)
            throws IOException {
        File file = new File("/tmp/actions.log");

        try ( BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), StandardCharsets.UTF_8)) ) {
            bw.write(data);
        }
    }

    public static void logQuarantined(String data)
            throws IOException {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        LocalDate dt = LocalDate.now();

        File file = new File("/tmp/quarantined-" + formatter.format(dt) + ".log");

        try ( BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), StandardCharsets.UTF_8)) ) {
            bw.write(data  + "\n");
        }
    }

}
