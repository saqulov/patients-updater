package mpi.util;

import mpi.actualizer.FileLine;
import mpi.actualizer.Patient;

import java.io.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;


public abstract class Excel {

    public static List<FileLine> Parse(String uri)  {
        List<FileLine> patients = new ArrayList<>();

        try {
            File inputF = new File(uri);

            InputStream inputFS = new FileInputStream(inputF);

            BufferedReader br = new BufferedReader(new InputStreamReader(inputFS));

            patients = br.lines().skip(1).map(mapToItem).collect(Collectors.toList());

            br.close();

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return patients;
    }

    private static Function<String, FileLine> mapToItem = (line) -> {
        DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .appendPattern("dd.MM.yyyy")
                .optionalStart()
                .appendPattern(" HH:mm")
                .optionalEnd()
                .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
                .parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0)
                .toFormatter();

        SimpleDateFormat format  = new SimpleDateFormat("dd.MM.yyyy");
        Patient patient = new Patient();
        FileLine fl = new FileLine();
        String[] p = line.split(";", -1);
        try {

            if(p[0].trim().length() != 0 && !p[0].equals("NULL"))
                fl.setAction(p[0]);

            if(p[1].trim().length() != 0 && !p[1].equals("NULL"))
                patient.setEnp(p[1]);

            if(p[2].trim().length() != 0 && !p[2].equals("NULL"))
                patient.setSnils(p[2]);

            if(p[3].trim().length() != 0 && !p[3].equals("NULL"))
                patient.setLastName(p[3]);

            if(p[4].trim().length() != 0 && !p[4].equals("NULL"))
                patient.setFirstName(p[4]);

            if(p[5].trim().length() != 0 && !p[5].equals("NULL"))
                patient.setMiddleName(p[5].trim());

            if(p[6].trim().length() != 0  && !p[6].equals("NULL"))
                patient.setBirthDate(format.parse(p[6]).toInstant()
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate());

            if(p[7].trim().length() != 0  && !p[7].equals("NULL"))
                fl.setBirthPlace(p[7]);

            patient.setGender(p[8].equals("M") ? "М" : p[8]);

            if(p[9].trim().length() != 0  && !p[9].equals("NULL"))
                fl.setRegistryAddress(p[9].trim());

            if(p[10].trim().length() != 0  && !p[10].equals("NULL"))
                fl.setRegistryFias(p[10].trim());

            if(p[11].trim().length() != 0 && !p[11].equals("NULL"))
                if(p[11].equals("совпадают")) fl.setAddress(p[9]);
                else fl.setAddress(p[11].trim());

            if(p[12].trim().length() != 0  && !p[12].equals("NULL"))
                fl.setFias(p[12]);

            if(p[13].trim().length() == 1)
                fl.setDpfsType(p[13]);

            if(p[14].trim().length() != 0 && !p[14].equals("NULL"))
                fl.setPolicySeries(p[14]);

            fl.setPolicyNumber(p[15]);

            if(p[16].trim().length() != 0  && !p[16].equals("NULL"))
                fl.setStart(LocalDateTime.parse(p[16], formatter));

            if(p[17].trim().length() != 0 && !p[17].equals("NULL"))
                fl.setSmo(p[17]);

            if(p[18].trim().length() != 0  && !p[18].equals("NULL"))
                fl.setTypeAttach(Integer.valueOf(p[18]));

            if(p[19].trim().length() != 0  && !p[19].equals("NULL"))
                fl.setAttach(LocalDateTime.parse(p[19], formatter));

            if(p[20].trim().length() != 0  && !p[20].equals("NULL"))
                fl.setUnAttach(LocalDateTime.parse(p[20], formatter));

            if(p[21].trim().length() != 0 && !p[21].equals("NULL"))
                fl.setPlaceCode(p[21]);

            if(p[22].trim().length() != 0 && !p[22].equals("NULL"))
                fl.setDocSNILS(p[22]);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            throw new RuntimeException();
        }

        fl.setPatient(patient);

        return fl;

    };
}
