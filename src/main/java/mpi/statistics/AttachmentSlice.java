package mpi.statistics;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AttachmentSlice {

    @SerializedName("moList")
    public List<MO> mos;

    @SerializedName("moListSize")
    private int moSize;

    public int getMoSize() {
        return moSize;
    }

    public AttachmentSlice() {
        mos = new ArrayList<>();
    }

    public void setMoSize(int moSize) {
        this.moSize = moSize;
    }

    public List<MO> getMos() {
        return mos;
    }

    public void setMos(List<MO> mos) {
        this.mos = mos;
        setMoSize(mos.size());
    }

}
