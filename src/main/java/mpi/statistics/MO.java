package mpi.statistics;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MO {

    @SerializedName("districtList")
    private List<District> districts;

    @SerializedName("id")
    private Long id;

    @SerializedName("name")
    private String name;

    @SerializedName("short")
    private String shortName;

    @SerializedName("oid")
    private String oid;

    @SerializedName("codeFOMS")
    private String codeFOMS;

    @SerializedName("districtListSize")
    private Integer size;

    public MO() {
        districts = new ArrayList<>();
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public List<District> getDistricts() {
        return districts;
    }

    public void setDistricts(List<District> districts) {
        this.districts = districts;
        this.size = districts == null ? 0 : districts.size();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getCodeFOMS() {
        return codeFOMS;
    }

    public void setCodeFOMS(String codeFOMS) {
        this.codeFOMS = codeFOMS;
    }
}
