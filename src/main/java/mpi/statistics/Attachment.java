package mpi.statistics;

import com.google.gson.annotations.SerializedName;

import java.time.LocalDateTime;

public class Attachment {

    @SerializedName("id")
    private Long id;

    @SerializedName("createDate")
    private LocalDateTime createDate;

    @SerializedName("updateDate")
    private LocalDateTime updateDate;

    @SerializedName("eventId")
    private Long eventId;

    @SerializedName("deleted")
    private Boolean deleted;

    @SerializedName("dateIn")
    private LocalDateTime dateIn;

    @SerializedName("dateOut")
    private LocalDateTime dateOut;

    @SerializedName("type")
    private Integer type;

    @SerializedName("person")
    private Long person;

    @SerializedName("doctor")
    private String doctor;

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    @SerializedName("district")
    private String district;

    public Attachment() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public LocalDateTime getDateIn() {
        return dateIn;
    }

    public void setDateIn(LocalDateTime dateIn) {
        this.dateIn = dateIn;
    }

    public LocalDateTime getDateOut() {
        return dateOut;
    }

    public void setDateOut(LocalDateTime dateOut) {
        this.dateOut = dateOut;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getPerson() {
        return person;
    }

    public void setPerson(Long person) {
        this.person = person;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

}
