package mpi.statistics;

import mpi.repos.AttachmentRepo;

public class Statistic {

    private AttachmentRepo repo;

    public Statistic()  {
    }

    public void setRepo(AttachmentRepo repo) {
        this.repo = repo;
    }
    public AttachmentSlice getSlice() {
        return repo.createSlice();
    }
}
