package mpi.statistics;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class District {

    @SerializedName("name")
    private String name;

    private transient List<Attachment> attachments;

    @SerializedName("attachmentListSize")
    private long size;

    public District() {
        attachments = new ArrayList<>();
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
        this.size = attachments == null ? 0 : attachments.size();
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }
}
