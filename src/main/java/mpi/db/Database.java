package mpi.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vibur.dbcp.ViburDBCPDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

public class Database {

    private static final Logger LOG = LoggerFactory.getLogger(Database.class);

    private ViburDBCPDataSource ds;
    //private Connection connection;
    List<PreparedStatement> statements;


    public Database() {
        ds = initDS();
    }

    private ViburDBCPDataSource initDS() {

        ViburDBCPDataSource ds = new ViburDBCPDataSource();
        statements = new ArrayList<>();

        //#region combat server
        ds.setJdbcUrl("jdbc:postgresql://127.0.0.1:5432/mpi?prepareThreshold=0&characterEncoding=utf8");
        ds.setUsername("mpi");
        ds.setPassword("&a9\"Dt<b5dV\\as%y");
        //#endregion

//        ds.setJdbcUrl("jdbc:postgresql://q4-demo2.medsoft.su:6432/mpi?prepareThreshold=0&characterEncoding=utf8");
//        ds.setUsername("mpi");
//        ds.setPassword("mpi");
        ds.setLogConnectionLongerThanMs(-1);
        ds.setLogLargeResultSet(-1);
        ds.start();
        return ds;
    }

    public void terminate() {

        ds.terminate();
    }

    private void prepareStatement(PreparedStatement ps, List<Object> params) throws SQLException {
        for (int i = 0; i < params.size(); i++) {
            ps.setObject(i + 1, params.get(i));
        }
    }

    //#region standard select

    public <T> T prepareSelect(Connection c, String sql, List<Object> params, Function<ResultSet, T> mapper) {
        try(PreparedStatement ps = c.prepareStatement(sql)) {
            prepareStatement(ps, params);
            return fetchData(ps, mapper);
        } catch (SQLException e) {
            LOG.error("ошибка при формировании");
            throw new RuntimeException("ошибка при формировании", e);
        }
    }
    public <T> T executeSelect(String sql, List<Object> params, Function<ResultSet, T> mapper) {
        try (Connection c = ds.getConnection()) {
            return prepareSelect(c, sql, params, mapper);
        } catch (SQLException e) {
            LOG.error("Ошибка при выполнении запроса к бд", e);
            throw new RuntimeException("Внутренняя ошибка сервера");
        }
    }

    private <T> T fetchData(PreparedStatement ps, Function<ResultSet, T> mapper) {
        try(ResultSet rs = ps.executeQuery()) {
            return mapper.apply(rs);
        } catch (SQLException e) {
            LOG.error("ошибка при получении данных из mpi.db");
            throw new RuntimeException("ошибка при получении данных из mpi.db", e);
        }
    }

    //#endregion

    //#region standard update
    public long executeUpdate(String sql, List<Object> params) {
        try (Connection c = ds.getConnection()) {
            return prepareUpdate(c, sql, params);
        } catch (SQLException e) {
            LOG.error("Ошибка при выполнении запроса к бд", e);
            throw new RuntimeException("Внутренняя ошибка сервера");
        }
    }
    private long prepareUpdate(Connection c, String sql, List<Object> params) throws SQLException {
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            prepareStatement(ps, params);
            return fetchUpdate(ps);
        }
    }
    private int fetchUpdate(PreparedStatement ps) throws SQLException {
        int affected = ps.executeUpdate();
        return affected;
    }

    //#endregion

    public void updateBatch(String sql, Set<Long> ids) throws RuntimeException {

        try (Connection c = ds.getConnection()) {
            try(PreparedStatement ps = c.prepareStatement(sql)) {
                for(Long id : ids) {
                    ps.setLong(1, id);
                    ps.addBatch();
                }
                ps.executeBatch();
            }
        } catch (SQLException e) {
            LOG.error("Ошибка при выполнении запроса к бд", e);
            throw new RuntimeException("Внутренняя ошибка сервера");
        }
    }
//    //#region transaction structure
//    public void destroyConnection() throws SQLException {
//        if(connection != null)
//            connection.close();
//    }
//    public Connection createConnection() throws SQLException {
//        Connection connection = ds.getConnection();
//        connection.setAutoCommit(false);
//        return connection;
//    }

//    public void setConnection(Connection connection) {
//        this.connection = connection;
//    }

//    public void closeConnection() throws SQLException {
//        this.connection.close();
//        this.connection = null;
//    }


//    public List<Long> executeTransaction() throws SQLException {
//        List<Long> keys = new ArrayList<>();
//
//        try {
//            connection.commit();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//
//            if (connection != null)
//                try {
//                System.err.println("Transaction is rolling back");
//                connection.rollback();
//
//            } catch (SQLException e1) {
//                e1.printStackTrace();
//                System.err.println(e1.getMessage());
//            } finally {
//                    for(PreparedStatement ps : statements)
//                        ps.close();
//                    statements.clear();
//                }
//        }
//        return keys;
//    }

//    public Long addStatement(String sql, List<Object> params, String type) throws SQLException {
//        Long result = (long) -1;
//        PreparedStatement ps;
//
//        if(type.equals("update")) {
//            ps = connection.prepareStatement(sql);
//            prepareStatement(ps, params);
//            ps.executeUpdate();
//        } else {
//            ps = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
//
//            try (ResultSet rs = ps.getGeneratedKeys()) {
//                rs.next();
//                result = rs.getLong(1);
//            }
//        }
//        statements.add(ps);
//
//        return result;
//    }

    //#endregion

    //#region standard insert
    public long executeInsert(String sql, List<Object> params) {
        try (Connection c = ds.getConnection()) {
            return prepareInsert(c, sql, params);
        } catch (SQLException e) {
            LOG.error("Ошибка при выполнении запроса к бд", e);
            throw new RuntimeException(e.getMessage());
        }
    }
    private long prepareInsert(Connection c, String sql, List<Object> params) throws SQLException {
        try (PreparedStatement ps = c.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)) {
            prepareStatement(ps, params);
            return fetchInsert(ps);
        }
    }
    private long fetchInsert(PreparedStatement ps) throws SQLException {
        ps.executeUpdate();

        try (ResultSet rs = ps.getGeneratedKeys()) {
            rs.next();
            return rs.getLong(1);
        }
    }

    //#endregion

}
