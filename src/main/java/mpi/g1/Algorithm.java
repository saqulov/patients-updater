package mpi.g1;

import mpi.repos.AttachmentRepo;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class Algorithm {

    private AttachmentRepo repo;
    public static Set<Long> toQuarantined = new HashSet<>();

    public Algorithm() {

    }

    public void setRepo(AttachmentRepo repo) {
        this.repo = repo;
    }
    public void setQuarantine() throws IOException {
        repo.setQuarantine();
    }

    public void removeQuarantined() throws IOException {
        repo.removeQuarantined();
    }
    public void undoQuarantine() throws RuntimeException {
        repo.undoQuarantineBatch(toQuarantined);
    }
}
